package fr.avatarreturns.moderation.bungee.utils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStreamReader;
import java.net.SocketAddress;
import java.net.URL;
import java.sql.ResultSet;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Utils {

    private final static Pattern ipPattern = Pattern
            .compile("(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)");

    private final static Pattern timePattern = Pattern.compile("(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?"
            + "(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?"
            + "(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?" + "(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?"
            + "(?:([0-9]+)\\s*(?:s[a-z]*)?)?", Pattern.CASE_INSENSITIVE);

    public static Optional<String> getUUID(final @NotNull String name) {
        try {
            final URL urlChecker = new URL("https://api.mojang.com/users/profiles/minecraft/" + name);
            final InputStreamReader inputStreamReader = new InputStreamReader(urlChecker.openStream());
            return Optional.ofNullable(new JsonParser().parse(inputStreamReader).getAsJsonObject().get("id").getAsString());
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public static String getIPCity(@NotNull String ip) {
        if (ip.equals("127.0.0.1"))
            ip = "";
        try {
            final URL geoApiURL = new URL("http://ip-api.com/json/" + ip + "?fields=country,city");
            final InputStreamReader inputStreamReader = new InputStreamReader(geoApiURL.openStream());
            final JsonObject object = new JsonParser().parse(inputStreamReader).getAsJsonObject();
            return object.get("city").getAsString() + " (" + object.get("country").getAsString() + ")";
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "inconnu";
    }

    public static String asList(final List<String> entry) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < entry.size(); i++) {
            stringBuilder.append("&3").append(entry.get(i));
            if (i < entry.size() - 1)
                stringBuilder.append("&7, ");
        }
        return stringBuilder.toString();
    }

    public static String asListNL(final List<String> entry) {
        final StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < entry.size(); i++) {
            stringBuilder.append("&3").append(entry.get(i));
            if (i < entry.size() - 1)
                stringBuilder.append("&7,%nl%¤¤");
        }
        return stringBuilder.toString();
    }

    public static String getName(final String uuid) {
        if (uuid.equals("CONSOLE"))
            return uuid;
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `players` WHERE `uuid` = '" + uuid + "';").get()) {
            if (resultSet.next())
                return resultSet.getString("name");
        }
        catch (Exception ignored) {
        }
        return null;
    }

    public static String obfIP(final String ip) {
        final String[] parts = ip.split("\\.");
        final StringBuilder builder = new StringBuilder();
        for (int i = 0; i < parts.length; i++) {
            for (int j = 0; j < parts[i].toCharArray().length; j++) {
                final int random = new Random().nextInt(20);
                String toAdd = String.valueOf(random);
                if (random > 10)
                    toAdd = "#";
                builder.append(toAdd);
            }
            if (i < parts.length - 1)
                builder.append(".");
        }
        return builder.toString();
    }

    public static String convertUUID(final UUID uuid) {
        return uuid.toString().replace("-", "");
    }

    public static String convertUUID(final String uuid) {
        return uuid.replace("-", "");
    }

    public static boolean validIP(final String ip) {
        return ipPattern.matcher(ip).matches();
    }

    public static String getPlayerIP(final ProxiedPlayer pp) {
        return getPlayerIP(pp.getSocketAddress());
    }

    public static String getPlayerIP(final SocketAddress socket) {
        return socket.toString().substring(1).split(":")[0];
    }

    public static @Nullable String getPlayerCachedIP(final String nameOrUUID) {
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `players` WHERE `uuid` = '" + nameOrUUID.replace("-", "") + "' OR `name` = '" + nameOrUUID + "';").get()) {
            if (resultSet.next())
                return resultSet.getString("last_ip");
        }
        catch (Exception ignored) {
        }
        return null;
    }

    public static @Nullable String playerExists(final String nameOrUUID) {
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `players` WHERE `uuid` = '" + nameOrUUID.replace("-", "") + "' OR `name` = '" + nameOrUUID + "';").get()) {
            if (resultSet.next())
                return resultSet.getString("uuid");
        }
        catch (Exception ignored) {
        }
        return null;
    }

    public static List<String> getPlayerNameByIP(final String ip) {
        final List<String> players = new ArrayList<>();
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `players` WHERE `last_ip` = '" + ip + "' ORDER BY `name` ASC;").get()) {
            while (resultSet.next())
                players.add(resultSet.getString("name"));
        } catch (Exception ignored) {
        }
        return players;
    }

    public static String dateFormat(final long timestamp) {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return getString(calendar);
    }

    public static String getString(final Calendar calendar) {
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        int seconds = calendar.get(Calendar.SECOND);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        return (hour < 10 ? "0" : "") + hour + ":" +
                (minutes < 10 ? "0" : "") + minutes + ":" +
                (seconds < 10 ? "0" : "") + seconds + " " +
                (day < 10 ? "0" : "") + day + "/" +
                (month < 10 ? "0" : "") + month + "/" +
                year;
    }

    public static String fromDuration(final long timeStamp) {
        final int millis = (int) Math.ceil(timeStamp / 1000D);
        final int seconds = (int) Math.ceil(millis % 60D);
        final int minutes = (int) Math.floor((millis / 60D) % 60D);
        final int hours = (int) Math.floor((millis / 3600D) % 24D);
        final int days = (int) Math.floor((millis / 3600D / 24D) % 30D);
        final int months = (int) Math.floor((millis / 3600D / 24D / 30D) % 12D);
        final int years = (int) Math.floor(millis / 3600D / 24D / 30D / 12D);
        final StringBuilder sb = new StringBuilder();
        if (years > 0)
            sb.append(years).append(" an").append(years > 1 ? "s" : "").append(isNotNull(1, months, days, hours, minutes, seconds) ? " et " : (!isNotNull(0, months, days, hours, minutes, seconds) ? ", " : ""));
        if (months > 0)
            sb.append(months).append(" mois").append(isNotNull(1, days, hours, minutes, seconds) ? " et " : (!isNotNull(0, days, hours, minutes, seconds) ? ", " : ""));
        if (days > 0)
            sb.append(days).append(" jour").append(days > 1 ? "s" : "").append(isNotNull(1, hours, minutes, seconds) ? " et " : (!isNotNull(0, hours, minutes, seconds) ? ", " : ""));
        if (hours > 0)
            sb.append(hours).append(" heure").append(hours > 1 ? "s" : "").append(isNotNull(1, minutes, seconds) ? " et " : (!isNotNull(0, minutes, seconds) ? ", " : ""));
        if (minutes > 0)
            sb.append(minutes).append(" minute").append(minutes > 1 ? "s" : "").append(isNotNull(1, seconds) ? " et " : "");
        if (seconds > 0)
            sb.append(seconds).append(" seconde").append(seconds > 1 ? "s" : "");
        if (sb.length() == 0)
            sb.append("maintenant");
        return sb.toString();
    }

    private static boolean isNotNull(final int numbers, final int... integers) {
        return Arrays.stream(integers).asDoubleStream().filter(number -> number != 0).count() == numbers;
    }

    public static long parseDuration(final String durationStr) {
        final Matcher m = timePattern.matcher(durationStr);
        int years = 0;
        int months = 0;
        int weeks = 0;
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        boolean found = false;
        while (m.find()) {
            if (m.group() == null || m.group().isEmpty()) {
                continue;
            }
            for (int i = 0; i < m.groupCount(); i++) {
                if (m.group(i) != null && !m.group(i).isEmpty()) {
                    found = true;
                    break;
                }
            }
            if (found) {
                if (m.group(1) != null && !m.group(1).isEmpty()) {
                    years = Integer.parseInt(m.group(1));
                }
                if (m.group(2) != null && !m.group(2).isEmpty()) {
                    months = Integer.parseInt(m.group(2));
                }
                if (m.group(3) != null && !m.group(3).isEmpty()) {
                    weeks = Integer.parseInt(m.group(3));
                }
                if (m.group(4) != null && !m.group(4).isEmpty()) {
                    days = Integer.parseInt(m.group(4));
                }
                if (m.group(5) != null && !m.group(5).isEmpty()) {
                    hours = Integer.parseInt(m.group(5));
                }
                if (m.group(6) != null && !m.group(6).isEmpty()) {
                    minutes = Integer.parseInt(m.group(6));
                }
                if (m.group(7) != null && !m.group(7).isEmpty()) {
                    seconds = Integer.parseInt(m.group(7));
                }
                break;
            }
        }
        if (!found)
            return -1;
        final Calendar c = new GregorianCalendar();
        if (years > 0) {
            c.add(Calendar.YEAR, years);
        }
        if (months > 0) {
            c.add(Calendar.MONTH, months);
        }
        if (weeks > 0) {
            c.add(Calendar.WEEK_OF_YEAR, weeks);
        }
        if (days > 0) {
            c.add(Calendar.DAY_OF_MONTH, days);
        }
        if (hours > 0) {
            c.add(Calendar.HOUR_OF_DAY, hours);
        }
        if (minutes > 0) {
            c.add(Calendar.MINUTE, minutes);
        }
        if (seconds > 0) {
            c.add(Calendar.SECOND, seconds);
        }
        return c.getTimeInMillis();
    }

    public static List<ProxiedPlayer> isOnline(final String playerOrIP) {
        return AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.getName().equals(playerOrIP) || Utils.convertUUID(pp.getUniqueId()).equals(playerOrIP) || Utils.getPlayerIP(pp).equals(playerOrIP)).collect(Collectors.toList());
    }

    public static long getPlayerLastLogin(final String nameOrUUID) {
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `players` WHERE `uuid` = '" + nameOrUUID.replace("-", "") + "' OR `name` = '" + nameOrUUID + "';").get()) {
            if (resultSet.next())
                return resultSet.getLong("last_login");
        }
        catch (Exception ignored) {
        }
        return 0;
    }

    public static long getPlayerFirstLogin(final String nameOrUUID) {
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `players` WHERE `uuid` = '" + nameOrUUID.replace("-", "") + "' OR `name` = '" + nameOrUUID + "';").get()) {
            if (resultSet.next())
                return resultSet.getLong("first_login");
        }
        catch (Exception ignored) {
        }
        return 0;
    }

    public static List<String> getPlayerNameHistory(final String uuid) {
        final List<String> names = new ArrayList<>();
        try {
            final URL url = new URL("https://api.mojang.com/user/profiles/" + uuid + "/names");
            final InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
            for (final JsonElement element : new JsonParser().parse(inputStreamReader).getAsJsonArray()) {
                names.add(element.getAsJsonObject().get("name").getAsString());
            }
        } catch (Exception ignored) {
        }
        return names;
    }
}
