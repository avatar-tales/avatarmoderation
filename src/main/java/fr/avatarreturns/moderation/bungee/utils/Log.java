package fr.avatarreturns.moderation.bungee.utils;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.config.Messages;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;

import java.util.List;
import java.util.stream.Collectors;

public class Log {

    public static BaseComponent[] banPlayer(final String player, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_BAN_PLAYER.get(player, staff, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_BANNED_PLAYER.get(staff, reason);
    }

    public static BaseComponent[] banIP(final String ip, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_BAN_IP.get(Utils.obfIP(ip), staff, reason);
        final BaseComponent[] message = Messages.INFO_BAN_IP.get(ip, staff, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_BANNED_IP.get(staff, reason);
    }

    public static void commentPlayer(final String player, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_COMMENT_PLAYER.get(player, staff, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
    }

    public static void commentIP(final String ip, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_COMMENT_IP.get(Utils.obfIP(ip), staff, reason);
        final BaseComponent[] message = Messages.INFO_COMMENT_IP.get(ip, staff, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
    }

    public static BaseComponent[] kickPlayer(final String player, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_KICK_PLAYER.get(player, staff, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_KICKED_PLAYER.get(staff, reason);
    }

    public static BaseComponent[] kickIP(final String ip, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_KICK_IP.get(Utils.obfIP(ip), staff, reason);
        final BaseComponent[] message = Messages.INFO_KICK_IP.get(ip, staff, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_KICKED_IP.get(staff, reason);
    }

    public static BaseComponent[] mutePlayer(final String player, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_MUTE_PLAYER.get(player, staff, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_MUTED_PLAYER.get(staff, reason);
    }

    public static BaseComponent[] muteIP(final String ip, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_MUTE_IP.get(Utils.obfIP(ip), staff, reason);
        final BaseComponent[] message = Messages.INFO_MUTE_IP.get(ip, staff, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_MUTED_IP.get(staff, reason);
    }

    public static void pardonPlayer(final String player, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_UNBAN_PLAYER.get(player, staff, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
    }

    public static void pardonIP(final String ip, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_UNBAN_IP.get(Utils.obfIP(ip), staff, reason);
        final BaseComponent[] message = Messages.INFO_UNBAN_IP.get(ip, staff, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
    }

    public static BaseComponent[] tempBanPlayer(final String player, final String staff, final String reason, final String duration) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_BAN_TEMP_PLAYER.get(player, staff, duration, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_BANNED_TEMP_PLAYER.get(staff, duration, reason);
    }

    public static BaseComponent[] tempBanIP(final String ip, final String staff, final String reason, final String duration) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_BAN_TEMP_IP.get(Utils.obfIP(ip), staff, duration, reason);
        final BaseComponent[] message = Messages.INFO_BAN_TEMP_IP.get(ip, staff, duration, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_BANNED_TEMP_IP.get(staff, duration, reason);
    }

    public static BaseComponent[] tempMutePlayer(final String player, final String staff, final String reason, final String duration) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_MUTE_TEMP_PLAYER.get(player, staff, duration, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_MUTED_TEMP_PLAYER.get(staff, duration, reason);
    }

    public static BaseComponent[] tempMuteIP(final String ip, final String staff, final String reason, final String duration) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().parallelStream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_MUTE_TEMP_IP.get(Utils.obfIP(ip), staff, duration, reason);
        final BaseComponent[] message = Messages.INFO_MUTE_TEMP_IP.get(ip, staff, duration, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_MUTED_TEMP_IP.get(staff, reason);
    }

    public static BaseComponent[] unMutePlayer(final String player, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_UNMUTE_PLAYER.get(player, staff, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_UNMUTED_PLAYER.get(staff, reason);
    }

    public static BaseComponent[] unMuteIP(final String ip, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_UNMUTE_IP.get(Utils.obfIP(ip), staff, reason);
        final BaseComponent[] message = Messages.INFO_UNMUTE_IP.get(ip, staff, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_UNMUTED_IP.get(staff, reason);
    }

    public static String warnPlayer(final String player, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] message = Messages.INFO_WARN_PLAYER.get(player, staff, reason);
        toSend.forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_WARNED_PLAYER.getRaw(staff, reason);
    }

    public static String warnIP(final String ip, final String staff, final String reason) {
        final List<CommandSender> toSend = AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.hasPermission("avatar.moderation.see")).collect(Collectors.toList());
        toSend.add(AvatarModerationPlugin.get().getProxy().getConsole());
        final BaseComponent[] messageObf = Messages.INFO_WARN_IP.get(Utils.obfIP(ip), staff, reason);
        final BaseComponent[] message = Messages.INFO_WARN_IP.get(ip, staff, reason);
        toSend.stream().filter(commandSender -> !commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(messageObf));
        toSend.stream().filter(commandSender -> commandSender.hasPermission("avatar.moderation.see.ip")).forEach(commandSender -> commandSender.sendMessage(message));
        return Messages.INFO_WARNED_IP.getRaw(staff, reason);
    }

}
