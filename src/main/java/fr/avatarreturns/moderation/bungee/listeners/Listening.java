package fr.avatarreturns.moderation.bungee.listeners;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.commands.modules.Ban;
import fr.avatarreturns.moderation.bungee.config.Messages;
import fr.avatarreturns.moderation.bungee.storage.Config;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import fr.avatarreturns.moderation.bungee.users.User;
import fr.avatarreturns.moderation.bungee.utils.Utils;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.PostLoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.sql.ResultSet;

public class Listening implements Listener {

    @EventHandler
    public void onPreLogin(final LoginEvent e) {
        if (!AvatarModerationPlugin.get().isLoaded()) {
            e.setCancelled(true);
            e.setCancelReason(TextComponent.fromLegacyText(""));
        }
        Ban.BanResult result = Ban.isBan(Utils.convertUUID(e.getConnection().getUniqueId()));
        if (!result.isBanned())
            result = Ban.isBan(Utils.getPlayerIP(e.getConnection().getSocketAddress()));
        if (result.isBanned()) {
            e.setCancelled(true);
            if (result.isIp()) {
                if (result.isPermanent())
                    e.setCancelReason(Messages.INFO_BANNED_IP.get(result.getStaff(), result.getReason()));
                else
                    e.setCancelReason(Messages.INFO_BANNED_TEMP_IP.get(result.getStaff(), Utils.fromDuration(result.getEndTimeStamp() - System.currentTimeMillis()), result.getReason()));
            } else {
                if (result.isPermanent())
                    e.setCancelReason(Messages.INFO_BANNED_PLAYER.get(result.getStaff(), result.getReason()));
                else
                    e.setCancelReason(Messages.INFO_BANNED_TEMP_PLAYER.get(result.getStaff(), Utils.fromDuration(result.getEndTimeStamp() - System.currentTimeMillis()), result.getReason()));
            }
        }
    }

    @EventHandler
    public void onPlayerChat(final ChatEvent e) {
        boolean blockedCommand = Config.getInstance().getMuteCommandsCancelled().stream().anyMatch(entry -> e.getMessage().toLowerCase().startsWith(entry.toLowerCase()));
        if (e.getSender() instanceof ProxiedPlayer && (!e.getMessage().startsWith("/") || blockedCommand)) {
            User.getUsers().stream().filter(user -> user.getUuid().equals(((ProxiedPlayer) e.getSender()).getUniqueId())).filter(User::isMute).findFirst().ifPresent(user -> {
                e.setCancelled(true);
                if (user.isIs_ip()) {
                    if (user.getMute_end() == -1)
                        ((ProxiedPlayer) e.getSender()).sendMessage(Messages.INFO_MUTED_IP.get(user.getMute_staff(), user.getMute_reason()));
                    else
                        ((ProxiedPlayer) e.getSender()).sendMessage(Messages.INFO_MUTED_TEMP_IP.get(user.getMute_staff(), Utils.fromDuration(user.getMute_end() - System.currentTimeMillis()), user.getMute_reason()));
                } else {
                    if (user.getMute_end() == -1)
                        ((ProxiedPlayer) e.getSender()).sendMessage(Messages.INFO_MUTED_PLAYER.get(user.getMute_staff(), user.getMute_reason()));
                    else
                        ((ProxiedPlayer) e.getSender()).sendMessage(Messages.INFO_MUTED_TEMP_PLAYER.get(user.getMute_staff(), Utils.fromDuration(user.getMute_end() - System.currentTimeMillis()), user.getMute_reason()));
                }
            });
        }
    }

    @EventHandler
    public void onPlayerJoin(final PostLoginEvent e) {
        final ProxiedPlayer pp = e.getPlayer();
        if (Utils.playerExists(e.getPlayer().getUniqueId().toString()) == null) {
            DBConnection.getDatabase().modifyQuery("INSERT INTO `players` (`uuid`, `name`, `last_ip`, `first_login`, `last_login`) VALUES ('" + Utils.convertUUID(pp.getUniqueId()) + "', '" + pp.getName() + "', '" + Utils.getPlayerIP(pp) + "', '" + System.currentTimeMillis() + "', '" + System.currentTimeMillis() + "');");
        }
        else {
            DBConnection.getDatabase().modifyQuery("UPDATE `players` SET `name` = '" + pp.getName() + "', `last_ip` = '" + Utils.getPlayerIP(pp) + "', `last_login` = '" + System.currentTimeMillis() + "' WHERE `uuid` = '" + Utils.convertUUID(pp.getUniqueId()) + "';");
        }
        new Thread(() -> {
            try (final ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `comments` WHERE `comment_state` = '1' AND `comment_type` = 'WARN' AND (`comment_uuid` = '" + Utils.convertUUID(pp.getUniqueId()) + "' OR `comment_ip` = '" + Utils.getPlayerIP(pp) + "') ORDER BY `comment_id` ASC;").get()) {
                Thread.sleep(4 * 1000);
                while (resultSet.next()) {
                    if (!pp.isConnected())
                        break;
                    final String staff = Utils.getName(resultSet.getString("comment_staff"));
                    final String reason = resultSet.getString("comment_reason");
                    resultSet.getString("comment_ip");
                    String[] message;
                    if (resultSet.wasNull())
                        message = Messages.INFO_WARNED_PLAYER.getRaw(staff, reason).split("%subtitle%");
                    else
                        message = Messages.INFO_WARNED_IP.getRaw(staff, reason).split("%subtitle%");
                    final String title = message[0];
                    String subtitle = "";
                    if (message.length > 1)
                        subtitle = message[1];
                    pp.sendTitle(AvatarModerationPlugin.get().getProxy().createTitle().title(TextComponent.fromLegacyText(title)).subTitle(TextComponent.fromLegacyText(subtitle)).stay(20 * 5).fadeIn(3).fadeOut(4));
                    Thread.sleep(6 * 1000);
                    DBConnection.getDatabase().modifyQuery("UPDATE `comments` SET `comment_state` = '0' WHERE `comment_id` = '" + resultSet.getInt("comment_id") + "'");
                }
            } catch (Exception ignored) {
            }
        }).start();
        User.getUsers().add(new User(e.getPlayer()));
    }

    @EventHandler
    public void onPlayerQuit(final PlayerDisconnectEvent e) {
        User.getUsers().removeIf(user -> user.getUuid().equals(e.getPlayer().getUniqueId()));
    }
}
