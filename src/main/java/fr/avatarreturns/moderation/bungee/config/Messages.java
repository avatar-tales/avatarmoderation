package fr.avatarreturns.moderation.bungee.config;

import fr.avatarreturns.moderation.bungee.storage.Config;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;

public enum Messages {

    PREFIX_INFO("&3&lAvatarModération &8&l»&r"),
    PREFIX_ERROR("&c&lAvatarSecurity &4&l»&r"),
    PREFIX_TITLE("&c&l« &4&lAvatar Returns &c&l»"),
    PREFIX_AVERT("&3&lAvatar Returns &4&l»&r"),

    YES("&c&lOUI"),
    NO("&a&lNON"),

    INFO_BAN_PLAYER("%prefix_info% &3{0} &7a été banni par &3{1} &7pour : &3{2}"),
    INFO_BAN_IP("%prefix_info% &7L'IP &3{0} &7a été bannie par &3{1} &7pour : &3{2}"),
    INFO_BAN_TEMP_PLAYER("%prefix_info% &3{0} &7a été banni par &3{1} &7pendant &3{2} &7pour : &3{3}"),
    INFO_BAN_TEMP_IP("%prefix_info% &7L'IP &3{0} &7a été bannie par &3{1} &7pendant &3{2} &7pour : &3{3}"),
    INFO_BANNED_PLAYER("%prefix_title%%nl%%nl%%nl%&3{0} &7vous a banni pour :%nl%%nl%&3&l{1}"),
    INFO_BANNED_IP("%prefix_title%%nl%%nl%%nl%&3{0} &7a banni votre IP pour :%nl%%nl%&3&l{1}"),
    INFO_BANNED_TEMP_PLAYER("%prefix_title%%nl%%nl%%nl%&3{0} &7vous a banni%nl%pendant &3{1} &7pour :%nl%%nl%&3&l{2}"),
    INFO_BANNED_TEMP_IP("%prefix_title%%nl%%nl%%nl%&3{0} &7a banni votre IP%nl%pendant &3{1} &7pour :%nl%%nl%&3&l{2}"),
    INFO_UNBAN_PLAYER("%prefix_info% &3{0} &7vient d'être débanni par &3{1} &7pour : &3{2}"),
    INFO_UNBAN_IP("%prefix_info% &7L'IP &3{0} &7vient d'être débannie par &3{1} &7pour : &3{2}"),

    INFO_COMMENT_PLAYER("%prefix_info% &3{0} &7vient de recevoir un commentaire de &3{1} &7pour : &3{2}"),
    INFO_COMMENT_IP("%prefix_info% &7L'IP &3{0} &7vient de recevoir un commentaire de &3{1} &7pour : &3{2}"),
    INFO_COMMENT_RAW("&3Commentaire &l#{0} &8» &7Staff: &3{1}&7, date: &3{2}&7, &3{3}"),

    INFO_KICK_PLAYER("%prefix_info% &3{0} &7vient d'être expulsé par &3{1} &7pour : &3{2}"),
    INFO_KICK_IP("%prefix_info% &7L'IP &3{0} &7vient d'être expulsée par &3{1} &7pour : &3{2}"),
    INFO_KICKED_PLAYER("%prefix_title%%nl%%nl%%nl%&3{0} &7vous a expulsé pour :%nl%%nl%&3&l{1}"),
    INFO_KICKED_IP("%prefix_title%%nl%%nl%%nl%&3{0} &7a expulsé votre IP pour :%nl%%nl%&3&l{1}"),

    INFO_LOOKUP_IP("&f&m----&r &3Lookup &c{0} &f&m----&r%nl%" +
            "&7Emplacement de l'IP: &3{1}%nl%" +
            "&7Cette adresse IP est utilisée par les joueurs suivants :%nl%" +
            "¤&3{2}%nl%" +
            "&7Etat:%nl%" +
            "¤¤&c&lBannie&7: &3{3}%nl%" +
            "¤¤&c&lMute&7: &3{4}%nl%" +
            "&7Historique: &3{5} &7ban(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{6} &7mute(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{7} &7kick(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{8} &7commentaire(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{9} &7avertissement(s)%nl%" +
            "&f&m----&r &3Lookup &c{0} &f&m----"),
    INFO_LOOKUP_PLAYER("&f&m----&r &3Lookup &c{0} &f&m----%nl%" +
            "{1}%nl%" +
            "&7Etat:%nl%" +
            "¤¤&c&lBanni&7: &3{2}%nl%" +
            "¤¤&c&lBanni-IP&7: &3{3}%nl%" +
            "¤¤&c&lMute&7:&3 {4}%nl%" +
            "¤¤&c&lMute IP&7:&3 {5}%nl%" +
            "&7Première connexion: &3{6}%nl%" +
            "&7Dernière connexion: &3{7}%nl%" +
            "&7Dernière IP: &3{8}%nl%" +
            "&7Historique: &3{9} &7ban(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{10} &7mute(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{11} &7kick(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{12} &7commentaire(s)%nl%" +
            "¤¤¤¤¤¤¤¤¤¤¤¤&3{13} &7avertissement(s)%nl%" +
            "&7Trois derniers commentaires et/ou avertissements sur ce joueur:%nl%" +
            "¤¤&3{14}%nl%" +
            "&7Historique des changements de nom : &3{15}%nl%" +
            "&7UUID : &3{16}%nl%" +
            "&f&m----&r &3Lookup &c{0} &f&m----"),

    INFO_MUTE_PLAYER("%prefix_info% &3{0} &7est maintenant muet sous la décision de &3{1} &7car : &3{2}"),
    INFO_MUTE_IP("%prefix_info% &7L'IP &3{0} &7est maintenant muette sous la décision de &3{1} &7car : &3{2}"),
    INFO_MUTE_TEMP_PLAYER("%prefix_info% &3{0} &7est maintenant muet sous la décision de &3{1} &7pendant &3{2} &7car : &3{3}"),
    INFO_MUTE_TEMP_IP("%prefix_info% &7L'IP &3{0} &7est maintenant muette sous la décision de &3{1} &7pendant &3{2} &7car : &3{3}"),
    INFO_MUTED_PLAYER("%prefix_avert% &7Vous avez été rendu muet par &3{0} &7car : &3{1}"),
    INFO_MUTED_IP("%prefix_avert% &7Votre IP a été rendue muette par &3{0} &7car : &3{1}"),
    INFO_MUTED_TEMP_PLAYER("%prefix_avert% &7Vous avez été rendu muet par &3{0} &7pendant &3{1} &7car : &3{2}"),
    INFO_MUTED_TEMP_IP("%prefix_avert% &7Votre IP a été rendue muette par &3{0} &7pendant &3{1} &7car : &3{2}"),
    INFO_UNMUTE_PLAYER("%prefix_info% &3{0} &7vient d'être unmute par &3{1} &7pour : &3{2}"),
    INFO_UNMUTE_IP("%prefix_info% &7L'IP &3{0} &7vient d'être unmute par &3{1} &7pour : &3{2}"),
    INFO_UNMUTED_PLAYER("%prefix_info% &3{0} &7vous a rendu la parole pour : &3{1}"),
    INFO_UNMUTED_IP("%prefix_info% &7Votre IP a retrouvé la parole grâce à &3{0} &7&7pour : &3{1}"),

    INFO_PROCESSING("%prefix_info% &7Récupération des informations nécessaires à cette tâche..."),

    INFO_WARN_PLAYER("%prefix_info% &3{0} &7vient de recevoir un avertissement de &3{1} &7pour : &3{2}"),
    INFO_WARN_IP("%prefix_info% &7L'IP &3{0} &7vient de recevoir un avertissement de &3{1} &7pour : &3{2}"),
    INFO_WARNED_PLAYER("%prefix_title%%subtitle%&7Avertissement de &3{0} &7pour : &3{1}"),
    INFO_WARNED_IP("%prefix_title%%subtitle%&7IP avertie par &3{0} &7pour : &3{1}"),
    INFO_WARN_RAW("&cAvertissement &l#{0} &8» &7Staff: &3{1}&7, date: &3{2}&7, &3{3}"),


    ERROR_COMMANDS_ARGUMENT("%prefix_error% &cVous n'avez pas mis assez d'arguements."),
    ERROR_COMMAND_PERMISSION("%prefix_error% &cVous n'avez pas la permission d'exécuter cette commande."),
    ERROR_TIMESTAMP("%prefix_error% &cLe format de votre durée n'est pas valide."),
    ERROR_NOT__FOUND("%prefix_error% &cCe joueur n'est pas dans la base de donnée."),
    ERROR_ONLINE("%prefix_error% &cCe joueur n'est pas connecté."),

    ERROR_BAN_BEEN_PLAYER("%prefix_error% &cCe joueur est déjà banni."),
    ERROR_BAN_BEEN_IP("%prefix_error% &cCette IP est déjà bannie."),
    ERROR_BAN_BEEN_NOT_PLAYER("%prefix_error% &cCe joueur n'est pas banni."),
    ERROR_BAN_BEEN_NOT_IP("%prefix_error% &cCette IP est déjà bannie."),

    ERROR_MUTE_BEEN_PLAYER("%prefix_error% &cCe joueur est déjà muet."),
    ERROR_MUTE_BEEN_IP("%prefix_error% &cCette IP est déjà muette."),
    ERROR_MUTE_BEEN_NOT_PLAYER("%prefix_error% &cCe joueur n'est pas muet."),
    ERROR_MUTE_BEEN_NOT_IP("%prefix_error% &cCette IP n'est pas muette.");

    private String message;

    Messages(final String message) {
        this.message = message;
    }

    public BaseComponent[] get(final String... arguments) {
        return TextComponent.fromLegacyText(this.getRaw(arguments)
                .replace("%nl%", "\n"));
    }
    public String getFullRaw() {
        return this.message;
    }

    public String getRaw(final String... arguments) {
        String result = this.message;
        for (int i = 0; i < arguments.length; i++)
            result = result.replace("{" + i + "}", arguments[i]);
        return ChatColor.translateAlternateColorCodes('&', result
                .replace("%prefix_info%", PREFIX_INFO.message)
                .replace("%prefix_error%", PREFIX_ERROR.message)
                .replace("%prefix_title%", PREFIX_TITLE.message)
                .replace("%prefix_avert%", PREFIX_AVERT.message)
                .replace("¤", " ")
        );
    }

    public String[] getSplit(final String... arguments) {
        return this.getRaw(arguments).split("%nl%");
    }

    public static void init(final String path, final String name) {
        final File file = new File(path, name);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            for (final Messages messages : Messages.values()) {
                final String pathToMessage = messages.name().replace("__", "-").replace("_", ".").toLowerCase();
                if (!Config.addDefault(config, pathToMessage, messages.message)) {
                    messages.message = config.getString(pathToMessage);
                }
            }
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
        } catch (Exception ignored) {
        }
    }

}
