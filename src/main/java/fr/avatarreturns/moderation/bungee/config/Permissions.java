package fr.avatarreturns.moderation.bungee.config;

import fr.avatarreturns.moderation.bungee.storage.Config;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;

public enum Permissions {

    NONE(""),
    ADMIN("avatar.admin"),

    BAN_PLAYER("avatar.moderation.ban"),
    BAN_IP("avatar.moderation.ban.ip"),
    TEMP_BAN_PLAYER("avatar.moderation.tempban"),
    TEMP_BAN_IP("avatar.moderation.tempban.ip"),
    UNBAN_PLAYER("avatar.moderation.unban"),
    UNBAN_IP("avatar.moderation.unban.ip"),

    COMMENT_PLAYER("avatar.moderation.comment"),
    COMMENT_IP("avatar.moderation.comment.ip"),

    KICK_PLAYER("avatar.moderation.kick"),
    KICK_IP("avatar.moderation.kick.ip"),

    LOOKUP_PLAYER("avatar.moderation.lookup"),
    LOOKUP_IP("avatar.moderation.lookup.ip"),

    MUTE_PLAYER("avatar.moderation.mute"),
    MUTE_IP("avatar.moderation.mute.ip"),
    TEMP_MUTE_PLAYER("avatar.moderation.tempmute"),
    TEMP_MUTE_IP("avatar.moderation.tempmute.ip"),
    UNMUTE_PLAYER("avatar.moderation.unmute"),
    UNMUTE_IP("avatar.moderation.unmute.ip"),

    SEE_PLAYER("avatar.moderation.see"),
    SEE_IP("avatar.moderation.see.ip"),

    WARN_PLAYER("avatar.moderation.warn"),
    WARN_IP("avatar.moderation.warn.ip")
    ;

    private String permission;

    Permissions(String permission) {
        this.permission = permission;
    }

    public String get() {
        return this.permission;
    }

    public boolean has(final CommandSender sender) {
        return sender.hasPermission(Permissions.ADMIN.permission) || sender.hasPermission(this.permission);
    }

    public static void init(final String path, final String name) {
        final File file = new File(path, name);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final Configuration config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(file);
            for (final Permissions permissions : Permissions.values()) {
                if (permissions.permission.equals(""))
                    continue;
                final String pathToMessage = permissions.name().replace("__", "-").replace("_", ".").toLowerCase();
                if (!Config.addDefault(config, pathToMessage, permissions.permission)) {
                    permissions.permission = config.getString(pathToMessage);
                }
            }
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(config, file);
        } catch (Exception ignored) {
        }
    }

}
