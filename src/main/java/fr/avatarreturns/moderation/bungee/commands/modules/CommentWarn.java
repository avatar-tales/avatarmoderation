package fr.avatarreturns.moderation.bungee.commands.modules;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.config.Messages;
import fr.avatarreturns.moderation.bungee.config.Permissions;
import fr.avatarreturns.moderation.bungee.storage.Config;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import fr.avatarreturns.moderation.bungee.users.User;
import fr.avatarreturns.moderation.bungee.utils.Log;
import fr.avatarreturns.moderation.bungee.utils.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CommentWarn {

    public CommentWarn() {
        if (Config.getInstance().isCommentActive())
            AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new CommentCommand());
        if (Config.getInstance().isWarnActive())
            AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new WarnCommand());
    }

    public static class CommentCommand extends Command {

        public CommentCommand() {
            super("comment");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.COMMENT_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 2) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = Utils.convertUUID(((ProxiedPlayer) sender).getUniqueId());
            }
            if (isValidIP) {
                if (!Permissions.COMMENT_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                Log.commentIP(player, sender.getName(), reason.toString());
                DBConnection.getDatabase().modifyQuery("INSERT INTO `comments` (`comment_ip`, `comment_staff`, `comment_reason`, `comment_state`, `comment_type`, `comment_date`) VALUES ('" + player + "', '" + staff + "', '" + reason.toString() + "', '0', 'COMMENT', '" + System.currentTimeMillis() + "');");
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else {
                Log.commentPlayer(Utils.getName(uuid), sender.getName(), reason.toString());
                DBConnection.getDatabase().modifyQuery("INSERT INTO `comments` (`comment_uuid`, `comment_staff`, `comment_reason`, `comment_state`, `comment_type`, `comment_date`) VALUES ('" + uuid + "', '" + staff + "', '" + reason.toString() + "', '0', 'COMMENT', '" + System.currentTimeMillis() + "');");
            }
        }

    }

    public static class WarnCommand extends Command {

        public WarnCommand() {
            super("warn");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.WARN_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 2) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = Utils.convertUUID(((ProxiedPlayer) sender).getUniqueId());
            }
            if (isValidIP) {
                if (!Permissions.WARN_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                final String[] message = Log.warnIP(player, sender.getName(), reason.toString()).split("%subtitle%");
                final String title = message[0];
                String subtitle = "";
                if (message.length > 1)
                    subtitle = message[1];
                final int state = User.getUsers().stream().anyMatch(user -> user.getIp().equals(player)) ? 0 : 1;
                if (state == 0) {
                    final String finalSubtitle = subtitle;
                    User.getUsers().parallelStream().filter(user -> user.getIp().equals(player)).forEach(user -> user.getPlayer().sendTitle(AvatarModerationPlugin.get().getProxy().createTitle().title(TextComponent.fromLegacyText(title)).subTitle(TextComponent.fromLegacyText(finalSubtitle)).stay(20 * 5).fadeIn(3).fadeOut(4)));
                }
                DBConnection.getDatabase().modifyQuery("INSERT INTO `comments` (`comment_ip`, `comment_staff`, `comment_reason`, `comment_state`, `comment_type`, `comment_date`) VALUES ('" + player + "', '" + staff + "', '" + reason.toString() + "', '" + state + "', 'WARN', '" + System.currentTimeMillis() + "');");
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else {
                final String[] message = Log.warnPlayer(Utils.getName(uuid), sender.getName(), reason.toString()).split("%subtitle%");
                final String title = message[0];
                String subtitle = "";
                if (message.length > 1)
                    subtitle = message[1];
                final int state = User.getUsers().stream().anyMatch(user -> user.getUniqueId().equals(uuid)) ? 0 : 1;
                if (state == 0) {
                    final String finalSubtitle = subtitle;
                    User.getUsers().parallelStream().filter(user -> user.getUniqueId().equals(uuid)).forEach(user -> user.getPlayer().sendTitle(AvatarModerationPlugin.get().getProxy().createTitle().title(TextComponent.fromLegacyText(title)).subTitle(TextComponent.fromLegacyText(finalSubtitle)).stay(20 * 5).fadeIn(3).fadeOut(4)));
                }
                DBConnection.getDatabase().modifyQuery("INSERT INTO `comments` (`comment_uuid`, `comment_staff`, `comment_reason`, `comment_state`, `comment_type`, `comment_date`) VALUES ('" + uuid + "', '" + staff + "', '" + reason.toString() + "', '" + state + "', 'WARN', '" + System.currentTimeMillis() + "');");
            }
        }
    }

    public static int count(final String UUIDOrIP, final String type)  {
        int result = 0;
        String query = "SELECT * FROM `comments` WHERE `comment_type` = '" + type.toUpperCase() + "' AND (`comment_uuid` = '" + UUIDOrIP.replace("-", "") + "' OR `comment_ip` = '" + UUIDOrIP + "');";
        if (type.equals(""))
            query = "SELECT * FROM `comments` WHERE `comment_uuid` = '" + UUIDOrIP.replace("-", "") + "' OR `comment_ip` = '" + UUIDOrIP + "';";
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery(query).get()) {
            while (resultSet.next())
                result++;
        }
        catch (Exception ignored) {
        }
        return result;
    }

    public static List<String> retrieve(final String uuid, final String ip, final String type, final int max)  {
        String query = "SELECT * FROM `comments` WHERE `comment_type` = '" + type.toUpperCase() + "' AND (`comment_uuid` = '" + uuid.replace("-", "") + "' OR `comment_ip` = '" + ip + "') ORDER BY `comment_id` DESC;";
        if (type.equals(""))
            query = "SELECT * FROM `comments` WHERE `comment_uuid` = '" + uuid.replace("-", "") + "' OR `comment_ip` = '" + ip + "' ORDER BY `comment_id` DESC;";
        final List<String> results = new ArrayList<>();
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery(query).get()) {
            int index = 0;
            while (resultSet.next() && (max == -1 || index < max)) {
                if (resultSet.getString("comment_type").equals("COMMENT"))
                    results.add(Messages.INFO_COMMENT_RAW.getRaw(String.valueOf(resultSet.getInt("comment_id")), Utils.getName(resultSet.getString("comment_staff")), Utils.dateFormat(resultSet.getLong("comment_date")), resultSet.getString("comment_reason")));
                else
                    results.add(Messages.INFO_WARN_RAW.getRaw(String.valueOf(resultSet.getInt("comment_id")), Utils.getName(resultSet.getString("comment_staff")), Utils.dateFormat(resultSet.getLong("comment_date")), resultSet.getString("comment_reason")));
                index++;
            }
        }
        catch (Exception ignored) {
        }
        return results;
    }

}
