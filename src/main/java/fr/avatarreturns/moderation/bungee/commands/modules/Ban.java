package fr.avatarreturns.moderation.bungee.commands.modules;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.config.Messages;
import fr.avatarreturns.moderation.bungee.config.Permissions;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import fr.avatarreturns.moderation.bungee.users.User;
import fr.avatarreturns.moderation.bungee.utils.Log;
import fr.avatarreturns.moderation.bungee.utils.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;

public class Ban {

    public Ban() {
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new BanCommand());
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new TempBanCommand());
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new UnbanCommand());
    }

    public static class BanCommand extends Command {

        public BanCommand() {
            super("ban");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.BAN_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 2) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = Utils.convertUUID(((ProxiedPlayer) sender).getUniqueId());
            }
            if (isValidIP) {
                if (!Permissions.BAN_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                if (isBan(player).isBanned()) {
                    sender.sendMessage(Messages.ERROR_BAN_BEEN_IP.get());
                    return;
                }
                final BaseComponent[] message = Log.banIP(player, sender.getName(), reason.toString());
                User.getUsers().stream().filter(user -> user.getIp().equals(player)).forEach(user -> user.getPlayer().disconnect(message));
                DBConnection.getDatabase().modifyQuery("INSERT INTO `bans` (`ban_ip`, `ban_staff`, `ban_reason`, `ban_begin`) VALUES ('" + player + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "');");
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else if (!isBan(uuid).isBanned()) {
                final BaseComponent[] message = Log.banPlayer(Utils.getName(uuid), sender.getName(), reason.toString());
                User.getUsers().stream().filter(user -> user.getUniqueId().equals(uuid)).forEach(user -> user.getPlayer().disconnect(message));
                DBConnection.getDatabase().modifyQuery("INSERT INTO `bans` (`ban_uuid`, `ban_staff`, `ban_reason`, `ban_begin`) VALUES ('" + uuid + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "');");
            }
            else {
                sender.sendMessage(Messages.ERROR_BAN_BEEN_PLAYER.get());
            }
        }

    }

    public static class TempBanCommand extends Command {

        public TempBanCommand() {
            super("tempban");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.TEMP_BAN_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 3) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            final long timestamp = Utils.parseDuration(args[1]);
            if (timestamp == -1) {
                sender.sendMessage(Messages.ERROR_TIMESTAMP.get());
                return;
            }
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 2; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = Utils.convertUUID(((ProxiedPlayer) sender).getUniqueId());
            }
            if (isValidIP) {
                if (!Permissions.TEMP_BAN_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                if (isBan(player).isBanned()) {
                    sender.sendMessage(Messages.ERROR_BAN_BEEN_IP.get());
                    return;
                }
                final BaseComponent[] message = Log.tempBanIP(player, sender.getName(), reason.toString(), args[1]);
                User.getUsers().parallelStream().filter(user -> user.getIp().equals(player)).forEach(user -> user.getPlayer().disconnect(message));
                DBConnection.getDatabase().modifyQuery("INSERT INTO `bans` (`ban_ip`, `ban_staff`, `ban_reason`, `ban_begin`, `ban_end`) VALUES ('" + player + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "', '" + timestamp + "');");
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else if (!isBan(uuid).isBanned()) {
                final BaseComponent[] message = Log.tempBanPlayer(Utils.getName(uuid), sender.getName(), reason.toString(), Utils.fromDuration(timestamp - System.currentTimeMillis()));
                User.getUsers().parallelStream().filter(user -> user.getUniqueId().equals(uuid)).forEach(user -> user.getPlayer().disconnect(message));
                DBConnection.getDatabase().modifyQuery("INSERT INTO `bans` (`ban_uuid`, `ban_staff`, `ban_reason`, `ban_begin`, `ban_end`) VALUES ('" + uuid + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "', '" + timestamp + "');");
            }
            else {
                sender.sendMessage(Messages.ERROR_BAN_BEEN_PLAYER.get());
            }
        }
    }

    public static class UnbanCommand extends Command {

        public UnbanCommand() {
            super("unban", "", "pardon");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.UNBAN_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 2) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = Utils.convertUUID(((ProxiedPlayer) sender).getUniqueId());
            }
            BanResult result;
            if (isValidIP) {
                if (!Permissions.UNBAN_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                if (!(result = isBan(player)).isBanned()) {
                    sender.sendMessage(Messages.ERROR_BAN_BEEN_NOT_IP.get());
                    return;
                }
                DBConnection.getDatabase().modifyQuery("UPDATE `bans` SET `ban_state` = '0', `ban_unban_date` = '" + System.currentTimeMillis() + "', `ban_unban_staff` = '" + staff + "', `ban_unban_reason` = '" + reason.toString() + "' WHERE `ban_id` = '" + result.getId() + "';");
                Log.pardonIP(player, sender.getName(), reason.toString());
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else if ((result = isBan(uuid)).isBanned()) {
                DBConnection.getDatabase().modifyQuery("UPDATE `bans` SET `ban_state` = '0', `ban_unban_date` = '" + System.currentTimeMillis() + "', `ban_unban_staff` = '" + staff + "', `ban_unban_reason` = '" + reason.toString() + "' WHERE `ban_id` = '" + result.getId() + "';");
                Log.pardonPlayer(Utils.getName(uuid), sender.getName(), reason.toString());
            }
            else {
                sender.sendMessage(Messages.ERROR_BAN_BEEN_NOT_PLAYER.get());
            }
        }
    }

    public static BanResult isBan(final String UUIDOrIP) {
        final BanResult result = new BanResult();
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `bans` WHERE `ban_state` = '1' AND (`ban_uuid` = '" + UUIDOrIP.replace("-", "") + "' OR `ban_ip` = '" + UUIDOrIP + "') ORDER BY `ban_id` DESC;").get()) {
            while (resultSet.next()) {
                resultSet.getString("ban_ip");
                final boolean ip = !resultSet.wasNull();
                final long end = resultSet.getLong("ban_end");
                if (resultSet.wasNull()) {
                    result.setId(resultSet.getInt("ban_id")).setStaff(resultSet.getString("ban_staff")).setReason(resultSet.getString("ban_reason")).setIp(ip).setBanned(true).setPermanent(true);
                    continue;
                }
                if (System.currentTimeMillis() > end) {
                    DBConnection.getDatabase().modifyQuery("UPDATE `bans` SET `ban_state` = '0' WHERE `ban_id` = '" + resultSet.getInt("ban_id") + "';");
                } else {
                    result.setId(resultSet.getInt("ban_id")).setStaff(resultSet.getString("ban_staff")).setReason(resultSet.getString("ban_reason")).setIp(ip).setBanned(true).setPermanent(false).setEndTimeStamp(end);
                }
            }
            return result;
        }
        catch (Exception ignored) {
        }
        return result;
    }

    public static int count(final String UUIDOrIP)  {
        int bans = 0;
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `bans` WHERE `ban_uuid` = '" + UUIDOrIP.replace("-", "") + "' OR `ban_ip` = '" + UUIDOrIP + "';").get()) {
            while (resultSet.next())
                bans++;
        }
        catch (Exception ignored) {
        }
        return bans;
    }

    public static class BanResult {

        private int id = -1;
        private boolean banned = false;
        private boolean permanent = false;
        private boolean ip = false;
        private long endTimeStamp = 0;

        private String staff;
        private String reason;

        public int getId() {
            return this.id;
        }

        private BanResult setId(final int id) {
            this.id = id;
            return this;
        }

        public boolean isBanned() {
            return this.banned;
        }

        public BanResult setBanned(final boolean banned) {
            this.banned = banned;
            return this;
        }

        public boolean isPermanent() {
            return this.permanent;
        }

        public BanResult setPermanent(final boolean permanent) {
            this.permanent = permanent;
            return this;
        }

        public boolean isIp() {
            return ip;
        }

        public BanResult setIp(boolean ip) {
            this.ip = ip;
            return this;
        }

        public long getEndTimeStamp() {
            return this.endTimeStamp;
        }

        public BanResult setEndTimeStamp(final long endTimeStamp) {
            this.endTimeStamp = endTimeStamp;
            return this;
        }

        public String getStaff() {
            return staff;
        }

        public BanResult setStaff(String staff) {
            this.staff = staff;
            return this;
        }

        public String getReason() {
            return reason;
        }

        public BanResult setReason(String reason) {
            this.reason = reason;
            return this;
        }

    }

}
