package fr.avatarreturns.moderation.bungee.commands.modules;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.config.Messages;
import fr.avatarreturns.moderation.bungee.config.Permissions;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import fr.avatarreturns.moderation.bungee.users.User;
import fr.avatarreturns.moderation.bungee.utils.Log;
import fr.avatarreturns.moderation.bungee.utils.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.util.List;
import java.util.stream.Collectors;

public class Mute {

    public Mute() {
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new MuteCommand());
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new TempMuteCommand());
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new UnmuteCommand());
    }

    public static class MuteCommand extends Command {

        public MuteCommand() {
            super("mute");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.MUTE_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 2) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = ((ProxiedPlayer) sender).getUniqueId().toString().replace("-", "");
            }
            if (isValidIP) {
                if (!Permissions.MUTE_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                if (isMute(player).isMuted()) {
                    sender.sendMessage(Messages.ERROR_MUTE_BEEN_IP.get());
                    return;
                }
                final BaseComponent[] message = Log.muteIP(player, sender.getName(), reason.toString());
                DBConnection.getDatabase().modifyQuery("INSERT INTO `mutes` (`mute_ip`, `mute_staff`, `mute_reason`, `mute_begin`) VALUES ('" + player + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "');");
                User.getUsers().stream().filter(user -> user.getIp().equals(player)).forEach(user -> { user.setMuteFromResult(isMute(user.getUniqueId())); user.getPlayer().sendMessage(message);});
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else if (!isMute(uuid).isMuted()) {
                DBConnection.getDatabase().modifyQuery("INSERT INTO `mutes` (`mute_uuid`, `mute_staff`, `mute_reason`, `mute_begin`) VALUES ('" + uuid + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "');");
                User.getUsers().stream().filter(user -> user.getUniqueId().equals(uuid)).forEach(user -> { user.setMuteFromResult(isMute(user.getUniqueId())); user.getPlayer().sendMessage(Log.mutePlayer(Utils.getName(uuid), sender.getName(), reason.toString())); });
            }
            else {
                sender.sendMessage(Messages.ERROR_MUTE_BEEN_PLAYER.get());
            }
        }

    }

    public static class TempMuteCommand extends Command {

        public TempMuteCommand() {
            super("tempmute");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.TEMP_MUTE_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 3) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final long timestamp = Utils.parseDuration(args[1]);
            if (timestamp == -1) {
                sender.sendMessage(Messages.ERROR_TIMESTAMP.get());
                return;
            }
            final StringBuilder reason = new StringBuilder();
            for (int i = 2; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = ((ProxiedPlayer) sender).getUniqueId().toString().replace("-", "");
            }
            if (isValidIP) {
                if (!Permissions.TEMP_MUTE_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                if (isMute(player).isMuted()) {
                    sender.sendMessage(Messages.ERROR_MUTE_BEEN_IP.get());
                    return;
                }
                final BaseComponent[] message = Log.tempMuteIP(player, sender.getName(), reason.toString(), args[1]);
                DBConnection.getDatabase().modifyQuery("INSERT INTO `mutes` (`mute_ip`, `mute_staff`, `mute_reason`, `mute_begin`, `mute_end`) VALUES ('" + player + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "', '" + timestamp + "');");
                User.getUsers().stream().filter(user -> user.getIp().equals(player)).forEach(user -> { user.setMuteFromResult(isMute(user.getUniqueId())); user.getPlayer().sendMessage(message);});
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else if (!isMute(uuid).isMuted()) {
                final BaseComponent[] message = Log.tempMutePlayer(Utils.getName(uuid), sender.getName(), reason.toString(), args[1]);
                DBConnection.getDatabase().modifyQuery("INSERT INTO `mutes` (`mute_uuid`, `mute_staff`, `mute_reason`, `mute_begin`, `mute_end`) VALUES ('" + uuid + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "', '" + timestamp + "');");
                User.getUsers().stream().filter(user -> user.getUniqueId().equals(uuid)).forEach(user -> {user.setMuteFromResult(isMute(user.getUniqueId())); user.getPlayer().sendMessage(message); });
            }
            else {
                sender.sendMessage(Messages.ERROR_MUTE_BEEN_PLAYER.get());
            }
        }
    }

    public static class UnmuteCommand extends Command {

        public UnmuteCommand() {
            super("unmute");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.UNMUTE_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 2) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = ((ProxiedPlayer) sender).getUniqueId().toString().replace("-", "");
            }
            MuteResult result;
            if (isValidIP) {
                if (!Permissions.UNMUTE_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                if (!(result = isMute(player)).isMuted()) {
                    sender.sendMessage(Messages.ERROR_MUTE_BEEN_NOT_IP.get());
                    return;
                }
                DBConnection.getDatabase().modifyQuery("UPDATE `mutes` SET `mute_state` = '0', `mute_unmute_date` = '" + System.currentTimeMillis() +"', `mute_unmute_staff` = '" + staff + "', `mute_unmute_reason` = '" + reason.toString() + "' WHERE `mute_id` = '" + result.getId() + "';");
                final BaseComponent[] message = Log.unMuteIP(player, sender.getName(), reason.toString());
                User.getUsers().stream().filter(user -> user.getIp().equals(player)).forEach(user -> { user.resetMute(); user.getPlayer().sendMessage(message);});
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else if ((result = isMute(uuid)).isMuted()) {
                DBConnection.getDatabase().modifyQuery("UPDATE `mutes` SET `mute_state` = '0', `mute_unmute_date` = '" + System.currentTimeMillis() +"', `mute_unmute_staff` = '" + staff + "', `mute_unmute_reason` = '" + reason.toString() + "' WHERE `mute_id` = '" + result.getId() + "';");
                final BaseComponent[] message = Log.unMutePlayer(Utils.getName(uuid), sender.getName(), reason.toString());
                User.getUsers().parallelStream().filter(user -> user.getUniqueId().equals(uuid)).forEach(user -> { user.resetMute(); user.getPlayer().sendMessage(message);});
            }
            else {
                sender.sendMessage(Messages.ERROR_MUTE_BEEN_NOT_PLAYER.get());
            }
        }
    }

    public static MuteResult isMute(final String UUIDOrIP) {
        final MuteResult result = new MuteResult();
        List<User> users;
        if (Utils.validIP(UUIDOrIP))
            users = User.getUsers().stream().filter(user -> user.getIp().equals(UUIDOrIP)).collect(Collectors.toList());
        else
            users = User.getUsers().stream().filter(user -> user.getUniqueId().equals(UUIDOrIP)).collect(Collectors.toList());
        for (final User user : users) {
            if (user.isMute())
                return user.getMuteFromResult();
        }
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `mutes` WHERE `mute_state` = '1' AND (`mute_uuid` = '" + UUIDOrIP.replace("-", "") + "' OR `mute_ip` = '" + UUIDOrIP + "') ORDER BY `mute_id` DESC;").get()) {
            while (resultSet.next()) {
                resultSet.getString("mute_ip");
                final boolean ip = !resultSet.wasNull();
                final long end = resultSet.getLong("mute_end");
                if (resultSet.wasNull()) {
                    result.setId(resultSet.getInt("mute_id")).setIp(ip).setStaff(resultSet.getString("mute_staff")).setReason(resultSet.getString("mute_reason")).setMuted(true).setPermanent(true).setEnd(-1);
                    continue;
                }
                if (System.currentTimeMillis() > end) {
                    DBConnection.getDatabase().modifyQuery("UPDATE `mutes` SET `mute_state` = '0' WHERE `mute_id` = '" + resultSet.getInt("mute_id") + "';");
                } else {
                    result.setId(resultSet.getInt("mute_id")).setIp(ip).setStaff(resultSet.getString("mute_staff")).setReason(resultSet.getString("mute_reason")).setMuted(true).setEnd(end);
                }
            }
            return result;
        }
        catch (Exception ignored) {
        }
        return result;
    }

    public static int count(final String UUIDOrIP)  {
        int mutes = 0;
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `mutes` WHERE `mute_uuid` = '" + UUIDOrIP.replace("-", "") + "' OR `mute_ip` = '" + UUIDOrIP + "';").get()) {
            while (resultSet.next())
                mutes++;
        }
        catch (Exception ignored) {
        }
        return mutes;
    }

    public static class MuteResult {

        private int id = -1;
        private boolean muted = false;
        private boolean permanent = false;
        private boolean ip = false;
        private long end = 0;

        private String staff;
        private String reason;

        public int getId() {
            return this.id;
        }

        public MuteResult setId(final int id) {
            this.id = id;
            return this;
        }

        public boolean isMuted() {
            return this.muted;
        }

        public MuteResult setMuted(final boolean muted) {
            this.muted = muted;
            return this;
        }

        public boolean isPermanent() {
            return this.permanent;
        }

        public MuteResult setPermanent(final boolean permanent) {
            this.permanent = permanent;
            return this;
        }

        public boolean isIp() {
            return ip;
        }

        public MuteResult setIp(boolean ip) {
            this.ip = ip;
            return this;
        }

        public long getEnd() {
            return this.end;
        }

        public MuteResult setEnd(final long end) {
            this.end = end;
            return this;
        }

        public String getStaff() {
            return staff;
        }

        public MuteResult setStaff(String staff) {
            this.staff = staff;
            return this;
        }

        public String getReason() {
            return reason;
        }

        public MuteResult setReason(String reason) {
            this.reason = reason;
            return this;
        }
    }

}
