package fr.avatarreturns.moderation.bungee.commands;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.commands.modules.Ban;
import fr.avatarreturns.moderation.bungee.commands.modules.CommentWarn;
import fr.avatarreturns.moderation.bungee.commands.modules.Kick;
import fr.avatarreturns.moderation.bungee.commands.modules.Mute;
import fr.avatarreturns.moderation.bungee.config.Messages;
import fr.avatarreturns.moderation.bungee.config.Permissions;
import fr.avatarreturns.moderation.bungee.storage.Config;
import fr.avatarreturns.moderation.bungee.utils.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.Objects;

public class Lookup {

    public Lookup() {
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new LookupCommand());
    }

    public static class LookupCommand extends Command {

        public LookupCommand() {
            super("glookup");
        }

        @Override
        public void execute(final CommandSender sender, final String[] args) {
            if (!Permissions.LOOKUP_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 1) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            if (Utils.validIP(args[0])) {
                if (!Permissions.LOOKUP_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                final String ip = args[0];
                new Thread(() -> {
                    final String players = Utils.asList(Utils.getPlayerNameByIP(ip));
                    if (players.length() == 0) {
                        sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
                        return;
                    }
                    sender.sendMessage(Messages.INFO_PROCESSING.get());
                    final String city = Utils.getIPCity(ip);
                    final String isBan = (Ban.isBan(ip).isBanned() ? Messages.YES.getFullRaw() : Messages.NO.getFullRaw());
                    final String isMute = (Mute.isMute(ip).isMuted() ? Messages.YES.getFullRaw() : Messages.NO.getFullRaw());
                    final String bans = String.valueOf(Ban.count(ip));
                    final String mutes = String.valueOf(Mute.count(ip));
                    final String kicks = String.valueOf(Kick.count(ip));
                    final String comments = String.valueOf(CommentWarn.count(ip, "COMMENT"));
                    final String warns = String.valueOf(CommentWarn.count(ip, "WARN"));
                    for (final String messages : Messages.INFO_LOOKUP_IP.getSplit(ip, city, players, isBan, isMute, bans, mutes, kicks, comments, warns)) {
                        sender.sendMessage(TextComponent.fromLegacyText(messages));
                    }
                }).start();
            }
            else {
                String uuid = Utils.playerExists(args[0]);
                if (uuid == null) {
                    sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
                    return;
                }
                if (Config.getInstance().getEntries_with_immunity().contains(uuid) && !Permissions.ADMIN.has(sender)) {
                    sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
                    return;
                }
                sender.sendMessage(Messages.INFO_PROCESSING.get());
                final String player = Utils.getName(uuid);
                new Thread(() -> {
                    ProxiedPlayer pp = null;
                    for (final ProxiedPlayer players : AvatarModerationPlugin.get().getProxy().getPlayers()) {
                        if (Utils.convertUUID(players.getUniqueId()).equals(uuid)) {
                            pp = players;
                            break;
                        }
                    }
                    String connexion = "&8Déconnecté";
                    if (pp != null)
                        if (pp.isConnected())
                            connexion = "&7Connecté sur le serveur &3" + pp.getServer().getInfo().getName();
                    final String isBan = (Ban.isBan(uuid).isBanned() ? Messages.YES.getFullRaw() : Messages.NO.getFullRaw());
                    final String isBanIP = (Ban.isBan(Utils.getPlayerCachedIP(uuid)).isBanned() ? Messages.YES.getFullRaw() : Messages.NO.getFullRaw());
                    final String isMute = (Mute.isMute(uuid).isMuted() ? Messages.YES.getFullRaw() : Messages.NO.getFullRaw());
                    final String isMuteIP = (Mute.isMute(Utils.getPlayerCachedIP(uuid)).isMuted() ? Messages.YES.getFullRaw() : Messages.NO.getFullRaw());
                    final String firstConnexion = Utils.dateFormat(Utils.getPlayerFirstLogin(uuid));
                    final String lastConnexion = Utils.dateFormat(Utils.getPlayerLastLogin(uuid));
                    final String lastIp = (Permissions.SEE_IP.has(sender) ? Utils.getPlayerCachedIP(uuid) : Utils.obfIP(Objects.requireNonNull(Utils.getPlayerCachedIP(uuid))));
                    final String bans = String.valueOf(Ban.count(uuid));
                    final String mutes = String.valueOf(Mute.count(uuid));
                    final String kicks = String.valueOf(Kick.count(uuid));
                    final String comments = String.valueOf(CommentWarn.count(uuid, "COMMENT"));
                    final String warns = String.valueOf(CommentWarn.count(uuid, "WARN"));
                    final String retrievedCommentsWarns = Utils.asListNL(CommentWarn.retrieve(uuid, lastIp, "", 3));
                    final String nameHistory = Utils.asList(Utils.getPlayerNameHistory(uuid));
                    for (final String messages : Messages.INFO_LOOKUP_PLAYER.getSplit(player, connexion, isBan, isBanIP, isMute, isMuteIP, firstConnexion, lastConnexion, lastIp, bans, mutes, kicks, comments, warns, retrievedCommentsWarns, nameHistory, uuid)) {
                        sender.sendMessage(TextComponent.fromLegacyText(messages));
                    }
                }).start();
            }
        }
    }
}
