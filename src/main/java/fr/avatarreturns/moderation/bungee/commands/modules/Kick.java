package fr.avatarreturns.moderation.bungee.commands.modules;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.config.Messages;
import fr.avatarreturns.moderation.bungee.config.Permissions;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import fr.avatarreturns.moderation.bungee.utils.Log;
import fr.avatarreturns.moderation.bungee.utils.Utils;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.sql.ResultSet;
import java.util.List;

public class Kick {

    public Kick() {
        AvatarModerationPlugin.get().getProxy().getPluginManager().registerCommand(AvatarModerationPlugin.get(), new KickCommand());
    }

    public static class KickCommand extends Command {

        public KickCommand() {
            super("kick");
        }

        @Override
        public void execute(CommandSender sender, String[] args) {
            if (!Permissions.KICK_PLAYER.has(sender)) {
                sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                return;
            }
            if (args.length < 2) {
                sender.sendMessage(Messages.ERROR_COMMANDS_ARGUMENT.get());
                return;
            }
            final String player = args[0];
            String uuid;
            boolean isValidIP = Utils.validIP(player);
            final StringBuilder reason = new StringBuilder();
            for (int i = 1; i < args.length; i++) {
                reason.append(args[i]);
                if (i != args.length - 1)
                    reason.append(" ");
            }
            String staff = "CONSOLE";
            if (sender instanceof ProxiedPlayer) {
                staff = Utils.convertUUID(((ProxiedPlayer) sender).getUniqueId());
            }
            List<ProxiedPlayer> ppToKick;
            if (isValidIP) {
                if (!Permissions.KICK_IP.has(sender)) {
                    sender.sendMessage(Messages.ERROR_COMMAND_PERMISSION.get());
                    return;
                }
                if ((ppToKick = Utils.isOnline(player)).size() == 0) {
                    sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
                    return;
                }
                final BaseComponent[] message = Log.kickIP(player, sender.getName(), reason.toString());
                DBConnection.getDatabase().modifyQuery("INSERT INTO `kicks` (`kick_ip`, `kick_staff`, `kick_reason`, `kick_date`) VALUES ('" + player + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "');");
                ppToKick.forEach(pp -> pp.disconnect(message));
            }
            else if ((uuid = Utils.playerExists(player)) == null) {
                sender.sendMessage(Messages.ERROR_NOT__FOUND.get());
            } else if ((ppToKick = Utils.isOnline(player)).size() > 0) {
                final BaseComponent[] message = Log.kickPlayer(Utils.getName(uuid), sender.getName(), reason.toString());
                DBConnection.getDatabase().modifyQuery("INSERT INTO `kicks` (`kick_uuid`, `kick_staff`, `kick_reason`, `kick_date`) VALUES ('" + uuid + "', '" + staff + "', '" + reason.toString() + "', '" + System.currentTimeMillis() + "');");
                ppToKick.forEach(pp -> pp.disconnect(message));
            }
            else {
                sender.sendMessage(Messages.ERROR_ONLINE.get());
            }

        }
    }

    public static int count(final String UUIDOrIP)  {
        int kicks = 0;
        try (ResultSet resultSet = DBConnection.getDatabase().readQuery("SELECT * FROM `kicks` WHERE `kick_uuid` = '" + UUIDOrIP.replace("-", "") + "' OR `kick_ip` = '" + UUIDOrIP + "';").get()) {
            while (resultSet.next())
                kicks++;
        }
        catch (Exception ignored) {
        }
        return kicks;
    }

}
