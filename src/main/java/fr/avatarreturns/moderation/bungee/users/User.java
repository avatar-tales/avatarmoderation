package fr.avatarreturns.moderation.bungee.users;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;
import fr.avatarreturns.moderation.bungee.commands.modules.Mute;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import fr.avatarreturns.moderation.bungee.utils.Utils;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {

    private static final List<User> users = new ArrayList<>();
    private final UUID uuid;
    private final String ip;
    private int mute_id;
    private boolean isMute;
    private long mute_end;
    private boolean is_ip;
    private String mute_staff;
    private String mute_reason;

    private User() {
        this(UUID.randomUUID(), "");
    }

    public User(final ProxiedPlayer pp) {
        this.uuid = pp.getUniqueId();
        this.ip = Utils.getPlayerIP(pp);
        final Mute.MuteResult muteState = Mute.isMute(this.getUniqueId());
        this.mute_id = muteState.getId();
        this.isMute = muteState.isMuted();
        this.mute_end = muteState.getEnd();
        this.is_ip = muteState.isIp();
        this.mute_staff = muteState.getStaff();
        this.mute_reason = muteState.getReason();
    }

    public User(UUID uuid, String ip) {
        this.uuid = uuid;
        this.ip = ip;
    }

    public User(UUID uuid, String ip, int mute_id, boolean isMute, long mute_end, boolean is_ip, String mute_staff, String mute_reason) {
        this.uuid = uuid;
        this.ip = ip;
        this.mute_id = mute_id;
        this.isMute = isMute;
        this.mute_end = mute_end;
        this.is_ip = is_ip;
        this.mute_staff = mute_staff;
        this.mute_reason = mute_reason;
    }

    public static List<User> getUsers() {
        return users;
    }

    public ProxiedPlayer getPlayer() {
        return AvatarModerationPlugin.get().getProxy().getPlayers().stream().filter(pp -> pp.getUniqueId().equals(this.getUuid())).findFirst().get();
    }

    public UUID getUuid() {
        return this.uuid;
    }

    public String getUniqueId() {
        return Utils.convertUUID(this.uuid);
    }

    public String getIp() {
        return ip;
    }

    public int getMute_id() {
        return this.mute_id;
    }

    public void setMute_id(final int mute_id) {
        this.mute_id = mute_id;
    }

    public boolean isMute() {
        if (this.mute_id != -1 && this.mute_end < System.currentTimeMillis()) {
            System.err.println(this.mute_end);
            this.mute_end = 0;
            this.isMute = false;
            DBConnection.getDatabase().modifyQuery("UPDATE `mutes` SET `mute_state` = '0' WHERE `mute_id` = '" + this.mute_id + "';");
            this.mute_id = -1;
        }
        return isMute;
    }

    public void setMute(boolean mute) {
        this.isMute = mute;
    }

    public long getMute_end() {
        return this.mute_end;
    }

    public void setMute_end(long mute_end) {
        this.mute_end = mute_end;
    }

    public boolean isIs_ip() {
        return this.is_ip;
    }

    public void setIs_ip(boolean is_ip) {
        this.is_ip = is_ip;
    }

    public String getMute_staff() {
        return this.mute_staff;
    }

    public void setMute_staff(String mute_staff) {
        this.mute_staff = mute_staff;
    }

    public String getMute_reason() {
        return this.mute_reason;
    }

    public void setMute_reason(String mute_reason) {
        this.mute_reason = mute_reason;
    }

    public Mute.MuteResult getMuteFromResult() {
        return new Mute.MuteResult().setMuted(this.isMute()).setPermanent(this.getMute_end() == -1).setEnd(this.getMute_end()).setId(this.getMute_id()).setIp(this.isIs_ip()).setStaff(this.getMute_staff()).setReason(this.getMute_reason());
    }

    public void setMuteFromResult(Mute.MuteResult muteFromResult) {
        this.setMute_id(muteFromResult.getId());
        this.setMute(muteFromResult.isMuted());
        this.setMute_end(muteFromResult.getEnd());
        this.setIs_ip(muteFromResult.isIp());
        this.setMute_staff(muteFromResult.getStaff());
        this.setMute_reason(muteFromResult.getReason());
    }

    public void resetMute() {
        this.setMute_id(-1);
        this.setMute(false);
        this.setMute_end(0);
        this.setIs_ip(false);
        this.setMute_staff("");
        this.setMute_reason("");
    }

}
