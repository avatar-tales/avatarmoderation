package fr.avatarreturns.moderation.bungee;

import fr.avatarreturns.moderation.bungee.commands.Lookup;
import fr.avatarreturns.moderation.bungee.commands.modules.Ban;
import fr.avatarreturns.moderation.bungee.commands.modules.CommentWarn;
import fr.avatarreturns.moderation.bungee.commands.modules.Kick;
import fr.avatarreturns.moderation.bungee.commands.modules.Mute;
import fr.avatarreturns.moderation.bungee.config.Messages;
import fr.avatarreturns.moderation.bungee.config.Permissions;
import fr.avatarreturns.moderation.bungee.listeners.Listening;
import fr.avatarreturns.moderation.bungee.storage.Config;
import fr.avatarreturns.moderation.bungee.storage.DBConnection;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class AvatarModerationPlugin extends Plugin {

    private static AvatarModerationPlugin instance;

    public static AvatarModerationPlugin get() {
        return instance;
    }

    private boolean loaded;

    @Override
    public void onEnable() {
        super.onEnable();
        instance = this;
        this.loaded = false;
        this.getProxy().getPluginManager().registerListener(this, new Listening());
        Config.getInstance().init(this.getDataFolder().getAbsolutePath(), "config.yml");
        new Thread(() -> {
            if (!Config.getInstance().getStorage_engine().equalsIgnoreCase("MySQL")) {

            }
            DBConnection.init();
            Messages.init(this.getDataFolder().getAbsolutePath(), "messages.yml");
            Permissions.init(this.getDataFolder().getAbsolutePath(), "permissions.yml");
            if (Config.getInstance().isMuteActive())
                new Mute();
            if (Config.getInstance().isBanActive())
                new Ban();
            if (Config.getInstance().isKickActive())
                new Kick();
            if (Config.getInstance().isCommentActive() || Config.getInstance().isWarnActive())
                new CommentWarn();
            new Lookup();
            this.loaded = true;
        }).start();
    }

    public boolean loadSQLiteDriver(){
        final File driverPath = new File(this.getDataFolder() + File.separator + "lib" + File.separator + "sqlite_driver.jar");
        new File(this.getDataFolder() + File.separator + "lib").mkdir();

        if (!new File(this.getDataFolder() + File.separator + "lib" + File.separator + "sqlite_driver.jar").exists()) {
            this.getLogger().info("Téléchargement du driver SQLite-JDBC...");

            final String driverUrl = "https://repo1.maven.org/maven2/org/xerial/sqlite-jdbc/3.36.0.3/sqlite-jdbc-3.36.0.3.jar";
            try {
                final ReadableByteChannel rbc = Channels.newChannel(new URL(driverUrl).openStream());
                FileOutputStream fos = new FileOutputStream(driverPath);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                fos.close();
            } catch (final IOException e) {
                e.printStackTrace();
                return false;
            }

            getLogger().info("Driver SQLite-JDBC correctement téléchargé.");
        }

        // Load the driver
        try {
            final URL u = driverPath.toURI().toURL();
            final URLClassLoader systemClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
            final Class<URLClassLoader> sysclass = URLClassLoader.class;
            final Method method = sysclass.getDeclaredMethod("addURL", new Class[] { URL.class });
            method.setAccessible(true);
            method.invoke(systemClassLoader, u);

            Class.forName("org.sqlite.JDBC");
            return true;
        } catch (final Throwable t) {
            getLogger().severe("Driver SQLite-JDBC impossible à installer...");
            t.printStackTrace();
            return false;
        }
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public boolean isLoaded() {
        return loaded;
    }
}
