package fr.avatarreturns.moderation.bungee.storage;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class SQLite extends Database {

    private final String location;
    private final String database;
    private final File SQLfile;

    public SQLite(final String path, final String name) {
        this.database = name;
        this.location = path;
        final File folder = new File(this.location);
        if (!folder.exists()) {
            folder.mkdirs();
        }
        this.SQLfile = new File(folder.getAbsolutePath() + File.separator + this.database);
        this.open();
    }

    @Override
    public Connection open() {
        try {
            Class.forName("org.sqlite.JDBC");
            this.connection = DriverManager.getConnection("jdbc:sqlite:" + this.SQLfile.getAbsolutePath());
            return this.connection;
        } catch (final ClassNotFoundException | SQLException e) {
            e.printStackTrace();
            return null;
        }

    }

}
