package fr.avatarreturns.moderation.bungee.storage;

import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Config {

    private transient static Config instance;

    public static Config getInstance() {
        if (instance == null)
            instance = new Config();
        return instance;
    }

    private boolean modules__ban__enabled = true;
    private boolean modules__mute__enabled = true;
    private boolean modules__kick__enabled = true;
    private boolean modules__comment__enabled = true;
    private boolean modules__warn__enabled = true;

    private List<String> modules__mute__commands_cancelled = Arrays.asList("/msg", "/mp", "/w", "/whisper", "/tell");
    private List<String> modules__entries_with_immunity = Arrays.asList("127.0.0.1", "843633a84afc4bda969249b01355e413", "7ddd496aa66e48e38ecbcc477ffe5337");

    private final int storage__version = 1;
    private String storage__engine = "sqlite";
    private String storage__MySQL__host = "localhost";
    private String storage__MySQL__user = "user";
    private String storage__MySQL__password = "pass-w@rd!";
    private String storage__MySQL__database = "dbname";
    private int storage__MySQL__port = 3306;

    private transient File file;
    private transient Configuration config;

    private Config() {
    }

    public void init(final String path, final String name) {
        this.file = new File(path, name);
        try {
            if (!this.file.exists()) {
                this.file.getParentFile().mkdirs();
                this.file.createNewFile();
            }
            this.config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(this.file);
            for (final Field field : Config.class.getDeclaredFields()) {
                if (Modifier.isTransient(field.getModifiers()))
                    continue;
                final String pathField = field.getName().replace("__", ".");
                if (!addDefault(this.config, pathField, field.get(this)) && !Modifier.isFinal(field.getModifiers()))
                    field.set(this, this.config.get(pathField));
                else if (Modifier.isFinal(field.getModifiers()))
                    this.config.set(pathField, field.get(this));
            }
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(this.config, this.file);
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static boolean addDefault(final Configuration config, final String path, final Object o) {
        if (config.get(path) == null) {
            config.set(path, o);
            return true;
        }
        return false;
    }

    public boolean isBanActive() {
        return this.modules__ban__enabled;
    }

    public boolean isMuteActive() {
        return this.modules__mute__enabled;
    }

    public boolean isKickActive() {
        return this.modules__kick__enabled;
    }

    public boolean isCommentActive() {
        return this.modules__comment__enabled;
    }

    public boolean isWarnActive() {
        return this.modules__warn__enabled;
    }

    public List<String> getMuteCommandsCancelled() {
        return this.modules__mute__commands_cancelled;
    }

    public List<String> getEntries_with_immunity() {
        return modules__entries_with_immunity;
    }

    public int getStorageVersion() {
        return storage__version;
    }

    public String getStorage_engine() {
        return this.storage__engine;
    }

    public String getStorage_MySQL_host() {
        return this.storage__MySQL__host;
    }

    public String getStorage_MySQL_user() {
        return this.storage__MySQL__user;
    }

    public String getStorage_MySQL_password() {
        return this.storage__MySQL__password;
    }

    public String getStorage_MySQL_database() {
        return this.storage__MySQL__database;
    }

    public int getStorage_MySQL_port() {
        return this.storage__MySQL__port;
    }
}
