package fr.avatarreturns.moderation.bungee.storage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySQL extends Database {

    public MySQL() {
        this.open();
    }

    @Override
    public Connection open() {
        try {
            Class.forName("com.mysql.jdbc.Driver");

            final String url = "jdbc:mysql://" + Config.getInstance().getStorage_MySQL_host() + ":" + Config.getInstance().getStorage_MySQL_port() + "/" + Config.getInstance().getStorage_MySQL_database();

            this.connection = DriverManager.getConnection(url, Config.getInstance().getStorage_MySQL_user(), Config.getInstance().getStorage_MySQL_password());
            return this.connection;
        } catch (final ClassNotFoundException | SQLException e) {
            return null;
        }

    }


}
