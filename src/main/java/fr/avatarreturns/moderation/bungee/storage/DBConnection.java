package fr.avatarreturns.moderation.bungee.storage;

import fr.avatarreturns.moderation.bungee.AvatarModerationPlugin;

import java.util.ArrayList;
import java.util.List;

public class DBConnection {

    private static Database db;

    public static void init() {
        final boolean isMySQL = Config.getInstance().getStorage_engine().equalsIgnoreCase("MySQL");
        if (isMySQL)
            db = new MySQL();
        else
            db = new SQLite(AvatarModerationPlugin.get().getDataFolder().getAbsolutePath(), "AvatarBan.sql");
        try {
            assert db.isOpen();
        }
        catch (Exception e) {
            e.printStackTrace();
            return;
        }
        final List<String> tablesQueries = new ArrayList<>();
        tablesQueries.add(
                "CREATE TABLE IF NOT EXISTS `players` (" +
                        "`uuid` VARCHAR(36) PRIMARY KEY NOT NULL," +
                        "`name` VARCHAR(16)," +
                        "`last_ip` VARCHAR(16)," +
                        "`first_login` BIGINT," +
                        "`last_login` BIGINT" +
                ");");
        tablesQueries.add(
                "CREATE TABLE IF NOT EXISTS `bans` (" +
                        "`ban_id` INTEGER PRIMARY KEY AUTO" + (isMySQL ? "_" : "") + "INCREMENT," +
                        "`ban_uuid` VARCHAR(36)," +
                        "`ban_ip` VARCHAR(16)," +
                        "`ban_staff` VARCHAR(36)," +
                        "`ban_reason` VARCHAR(255)," +
                        "`ban_begin` BIGINT," +
                        "`ban_end` BIGINT," +
                        "`ban_state` TINYINT DEFAULT '1'," +
                        "`ban_unban_date` BIGINT," +
                        "`ban_unban_staff` VARCHAR(36)," +
                        "`ban_unban_reason` VARCHAR(255)" +
                ");");
        tablesQueries.add(
                "CREATE TABLE IF NOT EXISTS `mutes` (" +
                        "`mute_id` INTEGER PRIMARY KEY AUTO" + (isMySQL ? "_" : "") + "INCREMENT," +
                        "`mute_uuid` VARCHAR(36)," +
                        "`mute_ip` VARCHAR(16)," +
                        "`mute_staff` VARCHAR(36)," +
                        "`mute_reason` VARCHAR(255)," +
                        "`mute_begin` BIGINT," +
                        "`mute_end` BIGINT," +
                        "`mute_state` TINYINT DEFAULT '1'," +
                        "`mute_unmute_date` BIGINT," +
                        "`mute_unmute_staff` VARCHAR(36)," +
                        "`mute_unmute_reason` VARCHAR(255)" +
                ");");
        tablesQueries.add(
                "CREATE TABLE IF NOT EXISTS `kicks` (" +
                        "`kick_id` INTEGER PRIMARY KEY AUTO" + (isMySQL ? "_" : "") + "INCREMENT," +
                        "`kick_uuid` VARCHAR(36)," +
                        "`kick_ip` VARCHAR(16)," +
                        "`kick_staff` VARCHAR(36)," +
                        "`kick_reason` VARCHAR(255)," +
                        "`kick_date` BIGINT" +
                ");");
        tablesQueries.add(
                "CREATE TABLE IF NOT EXISTS `comments` (" +
                        "`comment_id` INTEGER PRIMARY KEY AUTO" + (isMySQL ? "_" : "") + "INCREMENT," +
                        "`comment_uuid` VARCHAR(36)," +
                        "`comment_ip` VARCHAR(16)," +
                        "`comment_staff` VARCHAR(36)," +
                        "`comment_reason` VARCHAR(255)," +
                        "`comment_state` TINYINT DEFAULT '1'," +
                        "`comment_type` VARCHAR(16)," +
                        "`comment_date` BIGINT" +
                ");");
        tablesQueries.forEach(query -> db.modifyQuery(query));
    }

    public static Database getDatabase() {
        return db;
    }

}
